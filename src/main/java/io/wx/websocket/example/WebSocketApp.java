/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.websocket.example;

import io.vertx.core.Handler;
import io.vertx.core.http.ServerWebSocket;
import io.wx.core3.config.BasicConfigurator;
import io.wx.core3.config.SimpleConfig;
import io.wx.core3.http.app.AbstractApplication;

/**
 *
 * @author moritz
 */
public class WebSocketApp extends AbstractApplication{

 
    
    public static void main(String[] args) throws Exception {
        WebSocketApp server = new WebSocketApp();
        server.start();
        
        server.getCore().getVertx().createHttpServer()
       .websocketHandler((ServerWebSocket ws) -> {
            server.logger.info("ws connection ? "+ws.uri()+"from "+ws.remoteAddress());
            ws.closeHandler(new Handler<Void>() {
                @Override
                public void handle(Void e) {
                    server.logger.info("socket disconnected");
                }
            });
        }).listen(1338);
        server.logger.info("websocket server on 1338");
        
        
        server.idle();
    }
    
    
    
    public WebSocketApp() throws Exception{
        super(new BasicConfigurator(new SimpleConfig().setPort(1337).setInstanceCount(3)));
    }
    
    
    @Override
    public String getApplicationPath() {
       return "io.wx.socketio.http";
    }
    
    
    
}
